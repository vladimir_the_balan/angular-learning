import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { User } from '../user.model';

@Component({
  selector: 'app-user-from',
  templateUrl: './user-from.component.html',
  styleUrls: ['./user-from.component.scss']
})
export class UserFromComponent implements OnInit {

  userForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]),
    age: new FormControl('',[Validators.required]),
    email: new FormControl('',[Validators.email])
  });
  
  @Output('onSubmit') submitEmiter = new EventEmitter<User>();
  constructor() { }

  ngOnInit(): void {
  }

  onSubmit() {
    let user = {
      name: this.userForm.get('name').value,
      age: this.userForm.get('age').value,
      email: this.userForm.get('email').value
    }
    this.submitEmiter.emit(user);
    this.userForm.reset();
  }

}
