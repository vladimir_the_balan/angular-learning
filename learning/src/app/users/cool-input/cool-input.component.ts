import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Comment } from '../comment.model';
import { CommentsService } from '../comments.service';

@Component({
  selector: 'app-cool-input',
  templateUrl: './cool-input.component.html',
  styleUrls: ['./cool-input.component.scss']
})
export class CoolInputComponent implements OnInit {

  comments: Comment[] = [];
  text = new FormControl('');

  constructor(private commentsService: CommentsService) { }

  ngOnInit(): void {
    this.commentsService.getComments()
      .subscribe((data:Comment[]) => {
        this.comments = data;
      });
  }

}
