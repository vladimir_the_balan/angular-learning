import { Component, OnInit } from '@angular/core';
import { User } from '../user.model';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users: User[] = [];

  constructor() { }

  ngOnInit(): void {
    // this.users.push({
    //   name: 'Vasile',
    //   age: 13,
    //   email: 'vasile@gmail'
    // });
  }

  addUser(user){
    this.users.push(user);
  } 

}
