import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserFromComponent } from './user-from/user-from.component';
import { UsersListComponent } from './users-list/users-list.component';
import { UsersComponent } from './users/users.component';
import { UsersRoutingModule } from './users-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CoolInputComponent } from './cool-input/cool-input.component';
import { HttpClientModule } from '@angular/common/http';
import { CommentsService } from './comments.service';


@NgModule({
  declarations: [
    UserFromComponent,
    UsersListComponent,
    UsersComponent,
    CoolInputComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    CommentsService
  ]
})
export class UsersModule { }
